# ${projectDir}/compose.yaml （docker-compose.yml）

```
services:

  mysql:
    # mysql 8.3 の最新版を利用する
    # 設定可能な値は [ https://hub.docker.com/_/mysql ] を参照
    image: "mysql:8.3"

    # 変数を設定する
    environment:
      # データベース名
      MYSQL_DATABASE: "${MYSQL_DATABASE}"
      # パスワード（root）
      MYSQL_ROOT_PASSWORD: "${MYSQL_ROOT_PASSWORD}"

      # 以下は 'spring-boot-docker-compose' 指定時は不要（設定すると逆に面倒）
      # → my.cnf 等で Trigger の作成権限を付与するなどが必須。
      # 接続アカウント
      # MYSQL_USER
      ♯ 接続パスワード
      # MYSQL_PASSWORD

    ports:
      # 接続ポートを利用する（アプリ開発時は必要）
      # 'spring-boot-docker-compose' 指定時は以下は不要かもしれない。
      # ex: jdbc:mysql://localhost:3306/mysql
      - 3306:3306

    volumes:
      # データを永続化する（PC再起動後等でも利用するなら必要）
      # 詳しくは https://matsuand.github.io/docs.docker.jp.onthefly/storage/volumes/ を参照
      - ./docker/mysql/data:/var/lib/mysql
      
      # 設定を保管する（設定をカスタマイズするなら必須）
      - ./docker/mysql/etc/my.cnf:/etc/my.cnf
      - ./docker/mysql/etc/mysql/conf.d:/etc/mysql/conf.d
```

# spring 連係
## ${projectDir}/build.gradle

```
// 省略
dependencies {
    // 省略
	implementation 'org.flywaydb:flyway-mysql'
	developmentOnly 'org.springframework.boot:spring-boot-docker-compose'
	runtimeOnly 'com.mysql:mysql-connector-j'
    // 省略
}
// 省略
```

## ${resourceDir}/application.yml

JDBCの接続とマイグレーションツール（Flyway）の設定をする
**datasource:** 全体もおそらく不要（一応 自動コミット安全のためOFFは明記する）

```
spring:
# 省略
    # データベース接続情報
    datasource:
# spring-boot-docker-compose を利用してる場合は設定不要
    #   url:            jdbc:mysql://localhost:3306/${databasename}
    #   username:       ${username}
    #   password:       ${password}
    #   基本的に設定不要（JDBC 仕様）
    #   driver-class-name:  
        hikari:
            auto-commit: false

    # マイグレーション時 データベース接続情報
    flyway:
        encoding:       UTF-8
# spring-boot-docker-compose を利用してる場合は設定不要
        # url:          ${spring.datasource.url}
        # user:         root
        # password:     ${root-password}
# 省略
```