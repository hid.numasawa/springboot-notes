# Controller

```java
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")

public class IndexController {

    @InitBinder
    void initBuilder(WebDataBinder binder) {
        // 文字列はtrimする。　ブランクはnull に変換する
        // 通常は @ControllerAdvice で共通定義を行う
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping
    public String get(@ModelAttribute("form") IndexForm form) {
        return "index";
    }

    @PostMapping
    public String post(@Validated @ModelAttribute("form") IndexForm form, BindingResult errors) {
        return "index";
    }

}
```

# Form
```java
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class IndexForm {

    @NotNull(message = "必須入力項目です。")
    @DateValid
    String date;
}
```

# Validation アノテーション
```java
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.*;
import jakarta.validation.*;

/**
 * カスタムバリデーション起動用アノテーション
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(DateValid.List.class)
@Documented
@Constraint(validatedBy = {
        DateValidator.class
})
public @interface DateValid {

    /**
     * 不正な日付（ 2023/04/31 等）の場合のフォーマットエラー時のエラーメッセージ。
     */
    String message() default "{com.example.spring.demo.DateValid.message}"; // 日付としては妥当ではありません。

    /**
     * dddd/dd/dd 形式ではない場合のエラーメッセージ。
     */
    String messagePattern() default "{com.example.spring.demo.DateValid.messagePattern}"; // 入力形式が誤っています

    /**
     * 文字列→日付（LocalDate）変換時のフォーマット。
     */
    String format() default "uuuu/MM/dd";

    /**
     * 入力フォーマット。 Pattern.regexp 同様。
     */
    String regexp() default "[\\d]{4}/[\\d]{2}/[\\d]{2}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        DateValid[] value();
    }
}
```

# validation 実装
```java
import java.time.*;
import java.time.chrono.JapaneseChronology;
import java.time.format.*;
import java.util.regex.Pattern;
import org.springframework.util.StringUtils;
import jakarta.validation.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateValidator implements ConstraintValidator<DateValid, String> {

    DateValid valid;
    DateTimeFormatter formatter;

    @Override
    public void initialize(DateValid valid) {
        this.valid = valid;
        this.formatter = DateTimeFormatter.ofPattern(valid.format())
                .withChronology(JapaneseChronology.INSTANCE)
                .withResolverStyle(ResolverStyle.STRICT);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (!StringUtils.hasText(value)) {
            log.debug("未入力のためスキップします。 {}", value);
            return true;
        }

        try {
            if (!Pattern.compile(this.valid.regexp()).matcher(value).matches()) {
                log.debug("入力形式が {} ではありませんでした。 vlaue={}", this.valid.regexp(), value);
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(this.valid.messagePattern()).addConstraintViolation();
                return false;
            }

            LocalDate.parse(value, this.formatter);
            log.info("正常処理 value={}", value);
            return true;

        } catch (DateTimeException e) {
            log.debug("日付としては不正でした。 vlaue={}", value);
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(this.valid.message()).addConstraintViolation();
            return false;
        }
    }
}
```
