検証・テストで利用する環境向け設定
 docker desktop 等のインストールが必須

# $PROJECT/build.gradle
```
dependencies {
# 省略
	developmentOnly 'org.springframework.boot:spring-boot-docker-compose'
# 省略
}
```

# $PROJECT/compose.yaml
docker-compose.yml と同様の記述
```yml
services:
  mysql:
    image: mysql
    environment:
      - 'MYSQL_DATABASE=spring'
      - 'MYSQL_PASSWORD=spring'
      - 'MYSQL_ROOT_PASSWORD=spring'
      - 'MYSQL_USER=spring'
    ports:
      - 3306:3306

  adminer:
    image: adminer
    ports:
      - 9999:8080
```


# $PROJECT\src\main\resources\application.yml
```
# 省略
spring:
# 省略
    datasource:
        hikari:
            jdbc-url:   ${DB_URL:jdbc:mysql://localhost:3306/spring}
            username:   ${DB_USERNAME:spring}
            password:   ${DB_PASSWORD:spring}
# 省略
```
