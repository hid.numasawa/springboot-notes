[利用規約](https://postcode.teraren.com/terms)

# Controller

```java
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")

public class IndexController {

    @InitBinder
    void initBuilder(WebDataBinder binder) {
        // 文字列はtrimする。　ブランクはnull に変換する
        // 通常は @ControllerAdvice で共通定義を行う
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }


    @GetMapping
    public String get(@ModelAttribute("form") ZipCodeForm form) {
        return "zipcode-valid";
    }

    @PostMapping
    public String post(@Validated @ModelAttribute("form") ZipCodeForm form, BindingResult errors) {
        return "zipcode-valid";
    }

}
```

# Form
```java
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ZipCodeForm {

    @NotNull(message = "必須入力項目です。")
    @DateValid
    String zipcode;
}
```

# Validation アノテーション
```java
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.*;
import jakarta.validation.*;

/**
 * 郵便番号チェックを行うカスタムバリデーション。
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(ZipCodeValid.List.class)
@Documented
@Constraint(validatedBy = {
        ZipCodeValidator.class
})
@Size(min = 7, max = 7)
@Pattern(regexp = "[\\d]+")
@ReportAsSingleViolation
public @interface ZipCodeValid {

    /**
     * エラーメッセージ
     */
    String message() default "{jp.mirageworld.spring.validation.demo.validation.ZipCode.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        ZipCodeValid[] value();
    }
}
```

# validation 実装
```java

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.client.*;
import com.fasterxml.jackson.annotation.*;
import jakarta.validation.*;
import lombok.extern.slf4j.Slf4j;

/**
 ＊ 郵便番号バリデーション実装（postcode.teraren.com）
 */ 
@Slf4j
public class ZipCodeValidator implements ConstraintValidator<ZipCodeValid, String> {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ZipCodeResponce extends HashMap<String, Object> {
    }

    private ZipCodeValid valid;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void initialize(ZipCodeValid valid) {
        this.valid = valid;
    }

    @Override
    public boolean isValid(String zipcode, ConstraintValidatorContext context) {
        if (StringUtils.hasText(zipcode)) {
            HttpStatusCode statusCode = HttpStatusCode.valueOf(400);
            try {
                // チェック
                ResponseEntity<ZipCodeResponce> responseEntity = this.restTemplate.getForEntity(
                        "https://postcode.teraren.com/postcodes/{zipcode}.json",
                        ZipCodeResponce.class,
                        zipcode);

                statusCode = responseEntity.getStatusCode();

            } catch (RestClientException e) {
                log.warn(e.getMessage(), e);
            }
            if (!statusCode.is2xxSuccessful()) {
                return this.errors(context, this.valid.message());
            }
        }

        return true;
    }

    boolean errors(ConstraintValidatorContext context, String message) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        return false;
    }
}
```
